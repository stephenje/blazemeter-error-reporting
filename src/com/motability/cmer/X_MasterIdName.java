package com.motability.cmer;

public class X_MasterIdName {
    private String masterId;
    private String masterName;
    private String reportNotes;

    public String getMasterId() {
        return this.masterId;
    }

    public void setMasterId(String masterId) {
        this.masterId = masterId;
    }

    public String getMasterName() {
        return this.masterName;
    }

    public void setMasterName(String masterName) {
        this.masterName = masterName;
    }

    public String getReportNotes() {
        return this.reportNotes;
    }

    public void setReportNotes(String reportNotes) {
        this.reportNotes= reportNotes;
    }

}
