package com.motability.cmer;

public class X_GlobalVariables {

    /* Declare a number of static variables for use globally */
    public static Integer slowResponseTimeThreshold = 1000;
    public static Integer pricingSlowResponseTimeThreshold = 3000;
    /* These are the bulk pricing tests response time measures */
    public static Integer amendModelPriceResponseTimeThreshold = 15000;
    public static Integer auditingServicePriceResponseTimeThreshold = 10000;

    /* These are for the regression tests */
    /* V5 */
    public static Integer messageQueueSlowResponseTimeThreshold = 120000;
    public static Integer dispatchReportSlowResponseTimeThreshold = 10000;
    public static Integer placementFileSlowResponseTimeThreshold = 50000;
    public static Integer picklistFileSlowResponseTimeThreshold = 50000;

    /* IdaaS */
    public static Integer idaasSlowResponseTimeThreshold = 300;


    public static final String BZM_API_KEY = "423939697f483f1c1fbebd85"; //0d3b6e5721441c9fcdd8aae0
    public static final String BZM_API_SECRET = "872b9917122a300a91bfe80f1289d5b83990f83d2600ba306197fca9bb73c9ddb65073d0"; //d6de7fe5ce86f62631185b060cc3671419423f7b7f7f5cfd9465e881834783f120da29a7

    /* These are temp values for testing purposes */
    public static final String BLAZEMETER_URL = "https://a.blazemeter.com/app/#/";

    /* we need to hold the number of tests, this is for use in the masterLimit variable for the soak test */
    public static final String TEST_COUNT="14";
}