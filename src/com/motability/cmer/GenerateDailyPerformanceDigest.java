package com.motability.cmer;

import org.json.simple.parser.ParseException;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;

public class GenerateDailyPerformanceDigest {
    public static StringBuilder digestSlackMessage = new StringBuilder();
    public static boolean digestIsPopulated = false;

    public static void main(String[] args) throws ParseException {
        String pipelineProjectName = args[0];
        String triggerJobName = args[1];
        String triggerJobNumber = args[2];
        String jobName = args[3];
        String jobNumber = args[4];
        String jenkinsURL = args[5];
        String testType = args[6];
        String serviceVersion = args[7];
        String gatewayVersion = args[8];
        String serviceName = args[9];

        switch(testType) {
            case "service-test":
                GenerateDailyPerformanceDigestService(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "regression-test":
                GenerateDailyPerformanceDigestService(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "peak-hour-test":
                GenerateDailyPerformanceDigestVolumeLarge(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "soak-test":
                GenerateDailyPerformanceDigestVolumeLarge(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "scalability-test":
                GenerateDailyPerformanceDigestVolumeLarge(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "smoke-test":
                GenerateDailyPerformanceDigestSmoke(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "volume-test":
                GenerateDailyPerformanceDigestVolumeSmall(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "concurrency-test":
                GenerateDailyPerformanceDigestVolumeLarge(pipelineProjectName, triggerJobName, triggerJobNumber, jobName, jobNumber, jenkinsURL, testType, serviceVersion, gatewayVersion, serviceName);
                break;
            case "no-response-test":
                System.out.println("no-response");
                break;
        }
    }

    public static void GenerateDailyPerformanceDigestService(String pipelineProjectName, String triggerJobName, String triggerJobNumber, String jobName, String jobNumber, String jenkinsURL, String testType, String serviceVersion, String gatewayVersion, String serviceName) throws ParseException {
    String projectId;
    String projectName;
    String workspaceId = null;
    String accountId;
    String testId;
    String testName;
    String masterId = null;
    String masterName = null;
    String averageTime = null;
    String minTime = null;
    String nthPercentile = null;
    String nfthPercentile = null;
    String nnthPercentile = null;
    String maxTime = null;
    String errorRate = null;
    String timeStarted = null;
    String hitRate = null;
    String prevAverageTime = null;
    String prevMinTime = null;
    String prevNthPercentile = null;
    String prevNfthPercentile = null;
    String prevNnthPercentile = null;
    String prevMaxTime = null;
    String prevErrorRate = null;
    String prevTimeStarted = null;
    String prevHitRate = null;
    String prevNotesDetails = null;
    String prevMasterName = null;

        /* Call the requestProjectWorkspaceId class */
        RequestProjectWorkspaceId getProjectWorkspaceDetails = new RequestProjectWorkspaceId();
        ArrayList<X_ProjectWorkspaceId>projectWorkspaceDetails = getProjectWorkspaceDetails.RequestProjectWorkspaceId(pipelineProjectName);

        /* We now have an array list with project id, workspace id and workspace name */
        /* We have to iterate this array for each project id which is effectively the api */
        for (X_ProjectWorkspaceId pwId: projectWorkspaceDetails) {
            /* Assign our details to variables to use to search for tests which are effectively each end point */
            projectId = pwId.getProjectId();
            workspaceId = pwId.getWorkspaceId();
            projectName = pwId.getProjectName();
            accountId = pwId.getAccountId();

            /* Now for each project we need go get the tests which are the endpoints, the Blazemeter API seems to return */
            /* every test regardless of the project if you provide so we will need to filter these here */
            /* Call the requestTestUdName class */
            RequestTestIdName getTestDetails = new RequestTestIdName();
            ArrayList<X_TestIdName>testDetails = getTestDetails.RequestTestIdName(projectId, workspaceId);

            /* The digest is built at a test level and therefore we need to track when we are in the first iteration of tests */
            /* so we can build the digest header i.e. pipeline names etc */
            int testEntryCount = 0;

            /* We now have an array list with test id and name */
            /* We have to iterate this and only pick out the tests we need for the project we are currently */
            /* iterating(as mentioned above the BlazeMeter API returns all tests) */
            for (X_TestIdName tidName: testDetails) {
                if(projectId.equals(tidName.getProjectId())) {
                    /* We need to ignore the PEAK-HOUR, SMOKE-TEST, SCALE and SOAK-TEST Tests */
                    if (!tidName.getTestName().equals("peak-hour") &&
                            !tidName.getTestName().equals("peak-hour-test") &&
                            !tidName.getTestName().equals("smoke-test") &&
                            !tidName.getTestName().equals("scalability-test") &&
                            !tidName.getTestName().equals("concurrency-test") &&
                            !tidName.getTestName().equals("soak-test")) {

                        /* We need to only include all services if the service value is all, otherwise we only select the service we are testing */
                        if(serviceName.equals("all") || (!serviceName.equals("all") && tidName.getTestName().equals(serviceName))) {
                            /* Assign our details to variables to use to search for test masters (reports) */
                            testId = tidName.getTestId();
                            testName = tidName.getTestName();

                            /* We increment the test entry count so we only build the slack digest on the first iteration */
                            testEntryCount++;

                            /* Now for every test we need to get the latest test master which is effectively the last report */
                            /* This is effectively the Jenkins Build No */
                            RequestMasterIdName getMasterDetails = new RequestMasterIdName();
                            ArrayList<X_MasterIdName> masterDetails = getMasterDetails.RequestMasterIdName(projectId, workspaceId, testId, "2");

                            /* Initialise a count variable to allow us to split current and previous results */
                            int repSummaryCount = 0;

                            /* We now have two (if two exist) test master id's (the latest one to be executed and the one before) */
                            /* We have to iterate this and pick out the report id and name so we can get the summary details */
                            for (X_MasterIdName midName : masterDetails) {

                                /* We set a count variable to determine when we are in the second loop */
                                repSummaryCount++;

                                /* Assign our details to variables to use to search for test masters (reports) summary details */
                                masterId = midName.getMasterId();

                                /* We are going to push the service version and gateway version numbers to the summary report */
                                PostMasterNotes getMasterNotes = new PostMasterNotes();
                                getMasterNotes.PostMasterNotes(masterId, serviceVersion, gatewayVersion);

                                /* Now for every test master we need to get the summary which provides the detail */
                                /* We iterate this and pick out the detail so we can build the digest */
                                RequestServiceSummaryDetails getSummaryDetails = new RequestServiceSummaryDetails();
                                ArrayList<X_ServiceSummaryDetails> summaryDetails = getSummaryDetails.RequestServiceSummaryDetails(masterId);

                                /* We write the masterName and prevMasterName here as we know them and if the test fails we do not write them */
                                /* later so they become nulls */
                                if (repSummaryCount == 1) {
                                    masterName = midName.getMasterName();
                                } else {
                                    prevMasterName = midName.getMasterName();
                                }

                                /* We now have the latest test master summary details and possibly the previous set if they exist */
                                /* We have to iterate this to get the information on the last executed test and previous one for each endpoint */
                                for (X_ServiceSummaryDetails rsDetails : summaryDetails) {
                                    /* Assign the details to variables so we can determine which ones exceed our required */
                                    /* response times and produce the digest */
                                    /* If we are in the first iteration we write to the current variables, if we are in the second iteration we write to the previous variables */
                                    if (repSummaryCount == 1) {
                                        averageTime = rsDetails.getAverageResponseTime();
                                        minTime = rsDetails.getMinResponseTime();
                                        nthPercentile = rsDetails.getNinetiethPercentileResponseTime();
                                        nfthPercentile = rsDetails.getNinetyFifthPercentileResponseTime();
                                        nnthPercentile = rsDetails.getNinetyNinthPercentileResponseTime();
                                        maxTime = rsDetails.getMaxResponseTime();
                                        errorRate = rsDetails.getErrorRate();
                                        timeStarted = rsDetails.getTestStartTime();
                                        hitRate = rsDetails.getHitRate();
                                    } else {
                                        prevAverageTime = rsDetails.getAverageResponseTime();
                                        prevMinTime = rsDetails.getMinResponseTime();
                                        prevNthPercentile = rsDetails.getNinetiethPercentileResponseTime();
                                        prevNfthPercentile = rsDetails.getNinetyFifthPercentileResponseTime();
                                        prevNnthPercentile = rsDetails.getNinetyNinthPercentileResponseTime();
                                        prevMaxTime = rsDetails.getMaxResponseTime();
                                        prevErrorRate = rsDetails.getErrorRate();
                                        prevTimeStarted = rsDetails.getTestStartTime();
                                        prevHitRate = rsDetails.getHitRate();
                                        prevNotesDetails = midName.getReportNotes();
                                    }
                                }
                            }
                            createSlackDigestServiceHeader(jenkinsURL, X_GlobalVariables.BLAZEMETER_URL, triggerJobName,
                                    triggerJobNumber, jobName, jobNumber, testEntryCount, projectName, testId, testName, averageTime,
                                    minTime, nthPercentile, nfthPercentile, nnthPercentile, maxTime, errorRate, timeStarted, hitRate, masterName, prevAverageTime,
                                    prevMinTime, prevNthPercentile, prevNfthPercentile, prevNnthPercentile, prevMaxTime, prevErrorRate, prevTimeStarted,
                                    prevHitRate, prevNotesDetails, prevMasterName, workspaceId, projectId, accountId, testType, serviceVersion, gatewayVersion,pipelineProjectName);

                            createSlackDigestService(jenkinsURL, X_GlobalVariables.BLAZEMETER_URL, triggerJobName,
                                    triggerJobNumber, jobName, jobNumber, testEntryCount, projectName, testId, testName, averageTime,
                                    minTime, nthPercentile, nfthPercentile, nnthPercentile, maxTime, errorRate, timeStarted, hitRate, masterName, prevAverageTime,
                                    prevMinTime, prevNthPercentile, prevNfthPercentile, prevNnthPercentile, prevMaxTime, prevErrorRate, prevTimeStarted,
                                    prevHitRate, prevNotesDetails, prevMasterName, workspaceId, projectId, accountId, testType, serviceVersion, gatewayVersion,pipelineProjectName);

                            /* Reset the variables */
                            masterName = null;
                            averageTime = null;
                            minTime = null;
                            nthPercentile = null;
                            nfthPercentile = null;
                            nnthPercentile = null;
                            maxTime = null;
                            errorRate = null;
                            timeStarted = null;
                            hitRate = null;
                            prevAverageTime = null;
                            prevMinTime = null;
                            prevNthPercentile = null;
                            prevNfthPercentile = null;
                            prevNnthPercentile = null;
                            prevMaxTime = null;
                            prevErrorRate = null;
                            prevTimeStarted = null;
                            prevHitRate = null;
                            prevNotesDetails = null;
                            prevMasterName = null;
                        }
                    }
                }
            }
        }
        /* Return the response if not populated return no-response */
        if(digestIsPopulated) {
            System.out.println(getDigestSlackMessage());
        }
        else {
            System.out.println("no-response");
        }
    }

    public static void GenerateDailyPerformanceDigestSmoke(String pipelineProjectName, String triggerJobName, String triggerJobNumber, String jobName, String jobNumber, String jenkinsURL, String testType, String serviceVersion, String gatewayVersion, String serviceName) throws ParseException {
        String projectId;
        String projectName;
        String workspaceId = null;
        String accountId;
        String testId;
        String testName;
        String masterId = null;
        String masterName = null;
        String responseCodesString="";

        /* Call the requestProjectWorkspaceId class */
        RequestProjectWorkspaceId getProjectWorkspaceDetails = new RequestProjectWorkspaceId();
        ArrayList<X_ProjectWorkspaceId>projectWorkspaceDetails = getProjectWorkspaceDetails.RequestProjectWorkspaceId(pipelineProjectName);

        /* We now have an array list with project id, workspace id and workspace name */
        /* We have to iterate this array for each project id which is effectively the api */
        for (X_ProjectWorkspaceId pwId: projectWorkspaceDetails) {
            /* Assign our details to variables to use to search for tests which are effectively each end point */
            projectId = pwId.getProjectId();
            workspaceId = pwId.getWorkspaceId();
            projectName = pwId.getProjectName();
            accountId = pwId.getAccountId();

            /* Now for each project we need go get the tests which are the endpoints, the Blazemeter API seems to return */
            /* every test regardless of the project if you provide so we will need to filter these here */
            /* Call the requestTestUdName class */
            RequestTestIdName getTestDetails = new RequestTestIdName();
            ArrayList<X_TestIdName>testDetails = getTestDetails.RequestTestIdName(projectId, workspaceId);

            /* The digest is built at a test level and therefore we need to track when we are in the first iteration of tests */
            /* so we can build the digest header i.e. pipeline names etc */
            int testEntryCount = 0;

            /* We now have an array list with test id and name */
            /* We have to iterate this and only pick out the tests we need for the project we are currently */
            /* iterating(as mentioned above the BlazeMeter API returns all tests) */
            for (X_TestIdName tidName: testDetails) {
                if(projectId.equals(tidName.getProjectId())){
                    /* We only want to pick out the SMOKE Test for the project we are in, the easiest way */
                    /* to do this is to just check for the name as if we change the class we will have to make */
                    /* wholesale changes to the other methods */
                    if(tidName.getTestName().equals(testType)) {
                        /* Assign our details to variables to use to search for test masters (reports) */
                        testId = tidName.getTestId();
                        testName = tidName.getTestName();

                        /* We increment the test entry count so we only build the slack digest on the first iteration */
                        testEntryCount++;

                        /* Now for every test we need to get the latest test master which is effectively the last report */
                        /* This is effectively the Jenkins Build No */
                        RequestMasterIdName getMasterDetails = new RequestMasterIdName();
                        ArrayList<X_MasterIdName> masterDetails = getMasterDetails.RequestMasterIdName(projectId, workspaceId, testId, X_GlobalVariables.TEST_COUNT);

                        /* Initialise a count variable to allow us to split current and previous results */
                        int repSummaryCount = 0;

                        /* We now have test master id's for each test (the latest one to be executed and the one before) */
                        /* We have to iterate this and pick out the report id and name so we can get the summary details */
                        for (X_MasterIdName midName : masterDetails) {

                            /* Assign our details to variables to use to search for test masters (reports) summary details */
                            masterId = midName.getMasterId();

                            /* We are going to push the service version and gateway version numbers to the summary report */
                            PostMasterNotes getMasterNotes = new PostMasterNotes();
                            getMasterNotes.PostMasterNotes(masterId, serviceVersion, gatewayVersion);

                            /* For every test master we need to get the response codes to determine if they return 200 */
                            /* We iterate this and pick out the distinct response codes to determine if each service is responding correctly */
                            RequestResponseCodesDistinct getResponseCodes = new RequestResponseCodesDistinct();
                            ArrayList<X_ResponseCodesDistinct>responseCodes = getResponseCodes.RequestResponseCodesDistinct(masterId);

                            /* We have an array of response codes for each test master, we are only interested in the latest set of test masters (we return 2 for the summary comparison) */
                            /* We iterate our array and as the API returns only distinct response codes we need to check to see if we have a 200 only, a 200 and other response codes or just other response codes */
                            for (X_ResponseCodesDistinct rcDistinct: responseCodes) {
                                /* We need to get the master name */
                                masterName = midName.getMasterName();

                                if(responseCodesString.length() == 0) {
                                    responseCodesString = rcDistinct.getResponseCode();
                                }
                                else {
                                    responseCodesString += ", " + rcDistinct.getResponseCode();
                                }
                            }
                            createSlackDigestSmoke(jenkinsURL, X_GlobalVariables.BLAZEMETER_URL, triggerJobName,
                                    triggerJobNumber, jobName, jobNumber, testEntryCount, projectName, testId,
                                    testName, masterName, workspaceId, projectId, accountId, testType,
                                    serviceVersion, gatewayVersion, responseCodesString);

                            /* Reset the variables */
                            masterName = null;
                            responseCodesString = "";
                        }
                    }
                }
            }
        }
        /* Return the response, if not populated return no-response */
        if(digestIsPopulated) {
            System.out.println(getDigestSlackMessage());
        }
        else {
            System.out.println("no-response");
        }
    }

    public static void GenerateDailyPerformanceDigestVolumeLarge(String pipelineProjectName, String triggerJobName, String triggerJobNumber, String jobName, String jobNumber, String jenkinsURL, String testType, String serviceVersion, String gatewayVersion, String serviceName) throws ParseException {
        String projectId;
        String projectName;
        String workspaceId = null;
        String accountId;
        String testId;
        String testName;
        String masterId = null;
        String masterName = null;
        String averageTime = null;
        String minTime = null;
        String nthPercentile = null;
        String nfthPercentile = null;
        String nnthPercentile = null;
        String maxTime = null;
        String errorRate = null;
        String timeStarted = null;
        String hitRate = null;
        String timeFinished = null;
        String testDuration = null;

        /* Call the requestProjectWorkspaceId class */
        RequestProjectWorkspaceId getProjectWorkspaceDetails = new RequestProjectWorkspaceId();
        ArrayList<X_ProjectWorkspaceId>projectWorkspaceDetails = getProjectWorkspaceDetails.RequestProjectWorkspaceId(pipelineProjectName);

        /* We now have an array list with project id, workspace id and workspace name */
        /* We have to iterate this array for each project id which is effectively the api */
        for (X_ProjectWorkspaceId pwId: projectWorkspaceDetails) {
            /* Assign our details to variables to use to search for tests which are effectively each end point */
            projectId = pwId.getProjectId();
            workspaceId = pwId.getWorkspaceId();
            projectName = pwId.getProjectName();
            accountId = pwId.getAccountId();

            /* Now for each project we need go get the tests which are the endpoints, the Blazemeter API seems to return */
            /* every test regardless of the project if you provide so we will need to filter these here */
            /* Call the requestTestUdName class */
            RequestTestIdName getTestDetails = new RequestTestIdName();
            ArrayList<X_TestIdName>testDetails = getTestDetails.RequestTestIdName(projectId, workspaceId);

            /* The digest is built at a test level and therefore we need to track when we are in the first iteration of tests */
            /* so we can build the digest header i.e. pipeline names etc */
            int testEntryCount = 0;

            /* We now have an array list with test id and name */
            /* We have to iterate this and only pick out the tests we need for the project we are currently */
            /* iterating(as mentioned above the BlazeMeter API returns all tests) */
            for (X_TestIdName tidName: testDetails) {
                if(projectId.equals(tidName.getProjectId())) {
                    /* We need to only use the PEAK-HOUR or CONCURRENCY Tests, unless we are using a label of VOLUME in which case we do need to iterate them */
                    if (tidName.getTestName().equals(testType)) {
                        /* Assign our details to variables to use to search for test masters (reports) */
                        testId = tidName.getTestId();
                        testName = tidName.getTestName();

                        /* We increment the test entry count so we only build the slack digest on the first iteration */
                        testEntryCount++;

                        /* Now for every test we need to get the latest test master which is effectively the last report */
                        /* This is effectively the Jenkins Build No */
                        RequestMasterIdName getMasterDetails = new RequestMasterIdName();
                        ArrayList<X_MasterIdName> masterDetails = getMasterDetails.RequestMasterIdName(projectId, workspaceId, testId, "1");

                        /* We now have two (if two exist) test master id's (the latest one to be executed and the one before) */
                        /* We have to iterate this and pick out the report id and name so we can get the summary details */
                        for (X_MasterIdName midName : masterDetails) {

                            /* Assign our details to variables to use to search for test masters (reports) summary details */
                            masterId = midName.getMasterId();

                            /* We are going to push the service version and gateway version numbers to the summary report */
                            PostMasterNotes getMasterNotes = new PostMasterNotes();
                            getMasterNotes.PostMasterNotes(masterId, serviceVersion, gatewayVersion);

                            /* Now for every test master we need to get the summary which provides the detail */
                            /* We iterate this and pick out the detail so we can build the digest */
                            RequestVolumeSummaryDetails getSummaryDetails = new RequestVolumeSummaryDetails();
                            ArrayList<X_VolumeSummaryDetails> summaryDetails = getSummaryDetails.RequestVolumeSummaryDetails(masterId);

                            /* We now have the latest test master summary details and possibly the previous set if they exist */
                            /* We have to iterate this to get the information on the last executed test and previous one for each endpoint */
                            for (X_VolumeSummaryDetails rsDetails : summaryDetails) {
                                /* Assign the details to variables so we can determine which ones exceed our required */
                                /* response times and produce the digest */
                                /* If we are in the first iteration we write to the current variables, if we are in the second iteration we write to the previous variables */
                                averageTime = rsDetails.getAverageResponseTime();
                                minTime = rsDetails.getMinResponseTime();
                                nthPercentile = rsDetails.getNinetiethPercentileResponseTime();
                                nfthPercentile = rsDetails.getNinetyFifthPercentileResponseTime();
                                nnthPercentile = rsDetails.getNinetyNinthPercentileResponseTime();
                                maxTime = rsDetails.getMaxResponseTime();
                                errorRate = rsDetails.getErrorRate();
                                timeStarted = rsDetails.getTestStartTime();
                                hitRate = rsDetails.getHitRate();
                                timeFinished = rsDetails.getTestFinishTime();
                                testDuration = rsDetails.getTestDuration();
                                masterName = rsDetails.getTestName();

                                createSlackDigestVolumeLarge(jenkinsURL, X_GlobalVariables.BLAZEMETER_URL, triggerJobName,
                                        triggerJobNumber, jobName, jobNumber, testEntryCount, projectName, testId, testName, averageTime,
                                        minTime, nthPercentile, nfthPercentile, nnthPercentile, maxTime, errorRate, timeStarted, hitRate,
                                        timeFinished, testDuration, masterName, masterId, workspaceId, projectId, accountId, testType,
                                        serviceVersion, gatewayVersion);

                                /* Reset the variables */
                                masterName = null;
                                averageTime = null;
                                minTime = null;
                                nthPercentile = null;
                                nfthPercentile = null;
                                nnthPercentile = null;
                                maxTime = null;
                                errorRate = null;
                                timeStarted = null;
                                hitRate = null;
                                timeFinished = null;
                                testDuration = null;
                                masterName = null;
                            }
                        }
                    }
                }
            }
        }
        /* Return the response */
        System.out.println(getDigestSlackMessage());
    }

    public static void GenerateDailyPerformanceDigestVolumeSmall(String pipelineProjectName, String triggerJobName, String triggerJobNumber, String jobName, String jobNumber, String jenkinsURL, String testType, String serviceVersion, String gatewayVersion, String serviceName) throws ParseException {
        String projectId;
        String projectName;
        String workspaceId = null;
        String accountId;
        String testId;
        String testName;
        String masterId = null;
        String masterName = null;
        String averageTime = null;
        String minTime = null;
        String nthPercentile = null;
        String nfthPercentile = null;
        String nnthPercentile = null;
        String maxTime = null;
        String errorRate = null;
        String timeStarted = null;
        String hitRate = null;
        String timeFinished = null;
        String testDuration = null;

        /* Call the requestProjectWorkspaceId class */
        RequestProjectWorkspaceId getProjectWorkspaceDetails = new RequestProjectWorkspaceId();
        ArrayList<X_ProjectWorkspaceId>projectWorkspaceDetails = getProjectWorkspaceDetails.RequestProjectWorkspaceId(pipelineProjectName);

        /* We now have an array list with project id, workspace id and workspace name */
        /* We have to iterate this array for each project id which is effectively the api */
        for (X_ProjectWorkspaceId pwId: projectWorkspaceDetails) {
            /* Assign our details to variables to use to search for tests which are effectively each end point */
            projectId = pwId.getProjectId();
            workspaceId = pwId.getWorkspaceId();
            projectName = pwId.getProjectName();
            accountId = pwId.getAccountId();

            /* Now for each project we need go get the tests which are the endpoints, the Blazemeter API seems to return */
            /* every test regardless of the project if you provide so we will need to filter these here */
            /* Call the requestTestUdName class */
            RequestTestIdName getTestDetails = new RequestTestIdName();
            ArrayList<X_TestIdName>testDetails = getTestDetails.RequestTestIdName(projectId, workspaceId);

            /* The digest is built at a test level and therefore we need to track when we are in the first iteration of tests */
            /* so we can build the digest header i.e. pipeline names etc */
            int testEntryCount = 0;

            /* We now have an array list with test id and name */
            /* We have to iterate this and only pick out the tests we need for the project we are currently */
            /* iterating(as mentioned above the BlazeMeter API returns all tests) */
            for (X_TestIdName tidName: testDetails) {
                if(projectId.equals(tidName.getProjectId())) {
                    /* We need to ignore the PEAK-HOUR, SMOKE-TEST, SCALE and SOAK-TEST Tests */
                    if (tidName.getTestName() != "peak-hour" ||
                            tidName.getTestName() != "smoke-test" ||
                            tidName.getTestName() != "scale" ||
                            tidName.getTestName() != "soak-test") {
                        /* If we are in the OLA:CA: project we need to filter on the pipeline project name and the trigger job name */
                        /* If the pipeline project name includes OLA:CA: and the test name includes the trigger job name minus the automated- part then we include */
                        if(pipelineProjectName.contains("OLA:CA:") && ((tidName.getTestName().contains(triggerJobName.substring((triggerJobName.indexOf("-") + 1),triggerJobName.length()))) || tidName.getTestName().equals("dvla-controller"))) {
                            /* Assign our details to variables to use to search for test masters (reports) */
                            testId = tidName.getTestId();
                            testName = tidName.getTestName();

                            /* We increment the test entry count so we only build the slack digest on the first iteration */
                            testEntryCount++;

                            /* Now for every test we need to get the latest test master which is effectively the last report */
                            /* This is effectively the Jenkins Build No */
                            RequestMasterIdName getMasterDetails = new RequestMasterIdName();
                            ArrayList<X_MasterIdName> masterDetails = getMasterDetails.RequestMasterIdName(projectId, workspaceId, testId, "1");

                            /* We now have two (if two exist) test master id's (the latest one to be executed and the one before) */
                            /* We have to iterate this and pick out the report id and name so we can get the summary details */
                            for (X_MasterIdName midName : masterDetails) {

                                /* Assign our details to variables to use to search for test masters (reports) summary details */
                                masterId = midName.getMasterId();

                                /* We are going to push the service version and gateway version numbers to the summary report */
                                //PostMasterNotes getMasterNotes = new PostMasterNotes();
                                //getMasterNotes.PostMasterNotes(masterId, serviceVersion, gatewayVersion);

                                /* Now for every test master we need to get the summary which provides the detail */
                                /* We iterate this and pick out the detail so we can build the digest */
                                RequestServiceSummaryDetails getSummaryDetails = new RequestServiceSummaryDetails();
                                ArrayList<X_ServiceSummaryDetails> summaryDetails = getSummaryDetails.RequestServiceSummaryDetails(masterId);

                                /* We now have the latest test master summary details and possibly the previous set if they exist */
                                /* We have to iterate this to get the information on the last executed test and previous one for each endpoint */
                                for (X_ServiceSummaryDetails rsDetails : summaryDetails) {
                                    /* Assign the details to variables so we can determine which ones exceed our required */
                                    /* response times and produce the digest */
                                    /* If we are in the first iteration we write to the current variables, if we are in the second iteration we write to the previous variables */
                                    averageTime = rsDetails.getAverageResponseTime();
                                    minTime = rsDetails.getMinResponseTime();
                                    nthPercentile = rsDetails.getNinetiethPercentileResponseTime();
                                    nfthPercentile = rsDetails.getNinetyFifthPercentileResponseTime();
                                    nnthPercentile = rsDetails.getNinetyNinthPercentileResponseTime();
                                    maxTime = rsDetails.getMaxResponseTime();
                                    errorRate = rsDetails.getErrorRate();
                                    timeStarted = rsDetails.getTestStartTime();
                                    hitRate = rsDetails.getHitRate();
                                    masterName = midName.getMasterName();
                                    testDuration = rsDetails.getTestDuration();
                                }
                            }
                            createSlackDigestVolumeSmall(jenkinsURL, X_GlobalVariables.BLAZEMETER_URL, triggerJobName,
                                    triggerJobNumber, jobName, jobNumber, testEntryCount, projectName, testId, testName, averageTime,
                                    minTime, nthPercentile, nfthPercentile, nnthPercentile, maxTime, errorRate, timeStarted, hitRate,
                                    masterName, testDuration, masterId, workspaceId, projectId, accountId, testType,
                                    serviceVersion, gatewayVersion);

                            /* Reset the variables */
                            masterName = null;
                            averageTime = null;
                            minTime = null;
                            nthPercentile = null;
                            nfthPercentile = null;
                            nnthPercentile = null;
                            maxTime = null;
                            errorRate = null;
                            timeStarted = null;
                            hitRate = null;
                            timeFinished = null;
                            testDuration = null;
                            masterName = null;
                        }
                    }
                }
            }
        }
        /* Return the response */
        System.out.println(getDigestSlackMessage());
    }

    public static boolean meetFailedCriteria(String averageTime, String errorRate, String pipelineName, String testName) {
        boolean returnVal;
        /* If we exceed 1000 milliseconds on the average response time or we have an error in the test we */
        /* always consider this a candidate for inclusion in the digest */
        if(pipelineName.equals("VRM:Pricing:e2e")){
            /* There are a couple of timing points that need to be measured against alternative response time, these are those */
            switch(testName) {
                case "amend-model-price-draft-service-bulk":
                    return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.amendModelPriceResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                case "show-pricing-auditing-service-breakdown":
                    return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.auditingServicePriceResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                default:
                    return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.pricingSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
            }
        }
        else {
            /* We are going to put a switch statement in to manage individual timing points for all regression */
            switch (pipelineName) {
                case "VRM:V5:REGRESSION":
                    /* We also have a nested swicth to measure each timing point individually */
                    switch(testName){
                        case "generate-v5-dispatch-report":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.dispatchReportSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                        case "search-v5-history-by-vehicle-id":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.slowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                        case "generate-v5-placement-file":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.placementFileSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                        case "generate-v5-picklist-file":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.picklistFileSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                        case "add-v5-dealer-sale-message-to-inbound-queue":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.messageQueueSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                        case "add-v5-auction-move-message-to-inbound-queue":
                            return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.messageQueueSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                    }
                case "IDAAS:ola-e2e:REGRESSION":
                    return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.idaasSlowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
                default:
                    return returnVal = (Integer.parseInt(averageTime) > X_GlobalVariables.slowResponseTimeThreshold || Integer.parseInt(errorRate) > 0) ? true : false;
            }
        }
    }

    public static void createSlackDigestServiceHeader(String jenkinsURL, String blazemeterURL, String triggerJobName, String triggerJobNumber,
                                                String jobName, String jobNumber, Integer testEntryCount, String projectName,
                                                String testId, String testName, String averageTime, String minTime, String nthPercentile,
                                                String nfthPercentile, String nnthPercentile, String maxTime, String errorRate, String timeStarted,
                                                String hitRate, String masterName, String prevAverageTime, String prevMinTime, String prevNthPercentile,
                                                String prevNfthPercentile, String prevNnthPercentile, String prevMaxTime, String prevErrorRate,
                                                String prevTimeStarted, String prevHitRate, String prevNotesDetails, String prevMasterName, String workspaceId,
                                                String projectId, String accountId, String testType, String serviceVersion, String gatewayVersion, String pipelineProjectName) {

        /* Initialise some string variable */
        String slackString = "";
        String errorRateString = "";

        /* To build the slack message we need to perform a number of activities, these being */
        /* In the first iteration we build the name of this pipeline, its job number and its status and the name of the trigger pipeline and its job number */
        if(digestSlackMessage.length() == 0) {
            /* Build the message header */
            /* If the master name is null then we have a problem and we output an error to slack */
            if(masterName != null) {
                /* Firstly we output the the name and status of the current pipeline (the performance one) */
                slackString += ": `" + jobName + "` #" + jobNumber + "\n";
                slackString += "<" + jenkinsURL + "blue/organizations/jenkins/" + jobName + "/detail/" + jobName + "/" + jobNumber + "/pipeline|Click Here To View Pipeline>\n";
                //slackString += "Performance Test Triggered By `" + triggerJobName + "` #" + triggerJobNumber + "\n";
                slackString += "<" + blazemeterURL + "accounts/" + accountId + "/workspaces/" + workspaceId + "/projects/" + projectId + "/tests|Click Here To View Performance Dashboard>\n";
                /* We only display the service version and gateway version of the value is not not-checked */
                if(!serviceVersion.equals("not-checked") && !gatewayVersion.equals("not-checked")) {
                    slackString += "Service Version: `" + serviceVersion + "` Gateway Version: `" + gatewayVersion + "`\n";
                }

                /* Write the header */
                digestSlackMessage.append(slackString);
                digestIsPopulated = true;
            }
        }

    }

    public static void createSlackDigestService(String jenkinsURL, String blazemeterURL, String triggerJobName, String triggerJobNumber,
                                         String jobName, String jobNumber, Integer testEntryCount, String projectName,
                                         String testId, String testName, String averageTime, String minTime, String nthPercentile,
                                         String nfthPercentile, String nnthPercentile, String maxTime, String errorRate, String timeStarted,
                                         String hitRate, String masterName, String prevAverageTime, String prevMinTime, String prevNthPercentile,
                                         String prevNfthPercentile, String prevNnthPercentile, String prevMaxTime, String prevErrorRate,
                                         String prevTimeStarted, String prevHitRate, String prevNotesDetails, String prevMasterName, String workspaceId,
                                         String projectId, String accountId, String testType, String serviceVersion, String gatewayVersion, String pipelineProjectName) {

        /* Initialise some string variable */
        String slackString = "";
        String errorRateString = "";

        /* To build the slack message we need to perform a number of activities, these being */
        /* In the first iteration we build the name of this pipeline, its job number and its status and the name of the trigger pipeline and its job number */
        if(digestSlackMessage.length() == 0) {
            /* Build the message header */
            /* If the master name is null then we have a problem and we output an error to slack */
            if(masterName != null) {
                /* Firstly we output the the name and status of the current pipeline (the performance one) */
                slackString += ": `" + jobName + "` #" + jobNumber + "\n";
                slackString += "<" + jenkinsURL + "blue/organizations/jenkins/" + jobName + "/detail/" + jobName + "/" + jobNumber + "/pipeline|Click Here To View Pipeline>\n";
                //slackString += "Performance Test Triggered By `" + triggerJobName + "` #" + triggerJobNumber + "\n";
                slackString += "<" + blazemeterURL + "accounts/" + accountId + "/workspaces/" + workspaceId + "/projects/" + projectId + "/tests|Click Here To View Performance Dashboard>\n";
                /* We only display the service version and gateway version of the value is not not-checked */
                if(!serviceVersion.equals("not-checked") && !gatewayVersion.equals("not-checked")) {
                    slackString += "Service Version: `" + serviceVersion + "` Gateway Version: `" + gatewayVersion + "`\n";
                }
            }
        }

        /* We now append the message for each endpoint */
        /* Assuming the masterName is not null */
        if(nfthPercentile != null) {
            /* Calculate error rate to two decimal places, if any */
            /* Calculate the error rate, if any */
            DecimalFormat df2 = new DecimalFormat(".##");
            /* We are unable to calculate the error percentage if we do not have any data, we check this here */
            if (errorRate != null && hitRate != null) {
                double errorRatePercent = (Double.parseDouble(errorRate) / Double.parseDouble(hitRate)) * 100;
                if (errorRate.equals("0")) {
                    errorRateString = "";
                } else {
                    errorRateString = "" + df2.format(errorRatePercent) + "%";
                }
            } else {
                errorRateString = "";
            }
            /* Do the current results meet their NFR, did the previous */
            /* If there are no previous we do not display a null */
            if(prevNfthPercentile != null) {
                /* If no errors then hide the error string */
                if(errorRateString.length() > 0) {
                    slackString += "*" + testName + "* `error: " + errorRateString + "`\n>`Test Date: " + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n>`Test Date: " + prevMasterName + " = " + prevAverageTime + "ms Avg | " + prevNfthPercentile + "ms 95%ile`\n";
                }
                else {
                    slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n>`Test Date: " + prevMasterName + " = " + prevAverageTime + "ms Avg | " + prevNfthPercentile + "ms 95%ile`\n";
                }
            }
            else {
                if(prevMasterName != null){
                    /* If no errors then hide the error string */
                    if(errorRateString.length() > 0) {
                        slackString += "*" + testName + "* `error: " + errorRateString + "`\n>`Test Date: " + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n>`Test Date: " + prevMasterName + " = failed`\n";
                    }
                    else {
                        slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n>`Test Date: " + prevMasterName + " = failed`\n";
                    }
                }
                else {
                    /* If no errors then hide the error string */
                    if(errorRateString.length() > 0) {
                        slackString += "*" + testName + "* `error: " + errorRateString + "`\n>`Test Date:" + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n";                    }
                    else {
                        slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = " + averageTime + "ms Avg | " + nfthPercentile + "ms 95%ile`\n";
                    }
                }
            }
        }
        else {
            /* Do the current results meet their NFR, did the previous */
            /* If there are no previous we do not display a null */
            if(prevNfthPercentile != null) {
                slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = failed`\n>`Test Date: " + prevMasterName + " = " + prevAverageTime + "ms Avg | " + prevNfthPercentile + "ms 95%ile`\n";
            }
            else {
                if (prevMasterName != null) {
                    slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = failed`\n>`Test Date: " + prevAverageTime + " = failed`\n";
                }
                else {
                    if(masterName != null) {
                        slackString += "*" + testName + "*\n>`Test Date: " + masterName + " = failed`\n";
                    }
                    else {
                        slackString += "*" + testName + "*\n>`no results`\n";
                    }
                }
            }
        }

        /* Add a newline to the end of each test */
        //slackString += "\n";

        /* Aggregate the output */
        /* Only if we exceed the error criteria at the 95th percentile */
        /* If both values are present in the results */
        if(nfthPercentile != null && errorRate != null) {
            if(meetFailedCriteria(nfthPercentile, errorRate, pipelineProjectName, testName)){
                digestSlackMessage.append(slackString);
                digestIsPopulated = true;
            }
        }
        /* If only the 95th percentile */
        else if(nfthPercentile != null){
            if(meetFailedCriteria(nfthPercentile, "0", pipelineProjectName, testName)){
                digestSlackMessage.append(slackString);
                digestIsPopulated = true;
            }
        }
        /* If only the error rate */
        else if(errorRate != null){
            if(meetFailedCriteria("0", errorRate, pipelineProjectName, testName)){
                digestSlackMessage.append(slackString);
                digestIsPopulated = true;
            }
        }
        /* If neither then it is always shown */
        else {
            digestSlackMessage.append(slackString);
            digestIsPopulated = true;
        }
    }

    public static void createSlackDigestSmoke(String jenkinsURL, String blazemeterURL, String triggerJobName, String triggerJobNumber,
                                             String jobName, String jobNumber, Integer testEntryCount, String projectName,
                                             String testId, String testName, String masterName, String workspaceId,
                                             String projectId, String accountId, String testType, String serviceVersion,
                                             String gatewayVersion, String responseCodeString) {

        /* Initialise some string variable */
        String slackString = "";

        /* To build the slack message we need to perform a number of activities, these being */
        /* In the first iteration we build the name of this pipeline, its job number and its status and the name of the trigger pipeline and its job number */
        if(digestSlackMessage.length() == 0) {
            /* Build the message header */
            /* If the master name is null then we have a problem and we output an error to slack */
            if(masterName != null) {
                /* Firstly we output the the name and status of the current pipeline (the performance one) */
                slackString += ": `" + jobName + "` #" + jobNumber + "\n";
                slackString += "<" + jenkinsURL + "blue/organizations/jenkins/" + jobName + "/detail/" + jobName + "/" + jobNumber + "/pipeline|Click Here To View Pipeline>\n";
                slackString += "Performance Test Triggered By `" + triggerJobName + "` #" + triggerJobNumber + "\n";
                slackString += "<" + blazemeterURL + "accounts/" + accountId + "/workspaces/" + workspaceId + "/projects/" + projectId + "/tests|Click Here To View Performance Dashboard>\n";
                slackString += "Service Version: `" + serviceVersion + "` Gateway Version: `" + gatewayVersion + "`\n";
            }
        }

        /* Build the smoke test string */
        slackString += "*" + masterName + "* `" + responseCodeString + "`\n";

        /* Add a newline to the end of each test */
        //slackString += "\n";

        /* Aggregate the output */
        digestSlackMessage.append(slackString);
    }

    public static void createSlackDigestVolumeLarge(String jenkinsURL, String blazemeterURL, String triggerJobName, String triggerJobNumber,
                                                String jobName, String jobNumber, Integer testEntryCount, String projectName,
                                                String testId, String testName, String averageTime, String minTime, String nthPercentile,
                                                String nfthPercentile, String nnthPercentile, String maxTime, String errorRate, String timeStarted,
                                                String hitRate, String timeFinished, String testDuration, String masterName,  String masterId, String workspaceId,
                                                String projectId, String accountId, String testType, String serviceVersion, String gatewayVersion) {

        /* Initialise some string variable */
        String slackString = "";
        String errorRateString = "";
        String durationString = "";
        String hitsPerSecondString = "";

        /* To build the slack message we need to perform a number of activities, these being */
        if(digestSlackMessage.length() == 0) {
            /* Build the message header */
            /* If the master name is null then we have a problem and we output an error to slack */
            if(masterName != null) {
                /* Firstly we output the the name and status of the current pipeline (the performance one) */
                slackString += ": `" + jobName + "` #" + jobNumber + "\n";
                slackString += "<" + jenkinsURL + "blue/organizations/jenkins/" + jobName + "/detail/" + jobName + "/" + jobNumber + "/pipeline|Click Here To View Pipeline>\n";
                //slackString += "Performance Test Triggered By `" + triggerJobName + "` #" + triggerJobNumber + "\n";
                slackString += "<" + blazemeterURL + "accounts/" + accountId + "/workspaces/" + workspaceId + "/projects/" + projectId + "/masters/" + masterId + "/summary|Click Here To View Performance Dashboard>\n";
                //slackString += "Service Version: `" + serviceVersion + "` Gateway Version: `" + gatewayVersion + "`\n";
                /* IF WE CAN GET A LINK TO THE OPENSHIFT REPORTING, CPU MEMORY ETC THEN PUT THE LINK HERE */
                /* ALSO THE QUEUE DEPTH REPORT ALSO PUT THE LINK HERE */
            }
        }

        /****** Block to always get information for each entry in the slack message ********/
        /* Calculate error rate to two decimal places, if any */
        /* Calculate the error rate, if any */
        DecimalFormat df2 = new DecimalFormat(".##");
        /* We are unable to calculate the error percentage if we do not have any data, we check this here */
        if (errorRate != null && hitRate != null) {
            double errorRatePercent = (Double.parseDouble(errorRate) / Double.parseDouble(hitRate)) * 100;
            if (errorRate.equals("0")) {
                errorRateString = "0%";
            } else {
                errorRateString = "" + df2.format(errorRatePercent) + "%";
            }
        } else {
            errorRateString = "0%";
        }

        /* Calculate the duration of the test in hours and mins */
        /* Get hours, if any */
        Integer durHours;
        Integer durMins;
        if(Integer.parseInt(testDuration) >= 3600) {
            durHours = Integer.parseInt(testDuration) / 3600;
            if (durHours > 0) {
                Integer durRemainder = Integer.parseInt(testDuration) - 3600;
                if (durRemainder > 0) {
                    durMins = durRemainder / 60;
                } else {
                    durMins = 0;
                }
                /* Build the test duration string */
                if (durMins > 0) {
                    durationString = durHours + " hours, " + durMins + " mins";
                } else {
                    durationString = durHours + " hours";
                }
            } else {
                durHours = 0;
                durMins = Integer.parseInt(testDuration);
                /* Build the test duration string */
                durationString = durMins + " mins";
            }
        }
        else {
            /* Less than an hour so we calculate the mins */
            durMins = Integer.parseInt(testDuration) / 60;
            durationString = durMins + " mins";
        }

        /* Calculate the transactions per second, to two decimal places */
        /* We are unable to calculate the error percentage if we do not have any data, we check this here */
        double hitsPerSecond = (Double.parseDouble(hitRate) / Double.parseDouble(testDuration));
        if (hitsPerSecond == 0.0) {
            hitsPerSecondString = "";
        } else {
            hitsPerSecondString = "" + df2.format(hitsPerSecond) + "trans/sec";
        }

        /****************************************************************/


            /* If the label is ALL then we get the duration and error rates from this array element as it holds the summary */
        if(masterName.equals("ALL")){
            /* We now write out the overall test information before iterating over the timing points */
            slackString += "*test details*\n>";
            /* Define a date format */
            LocalDateTime startDateTime = LocalDateTime.ofEpochSecond(Long.parseLong(timeStarted), 0, ZoneOffset.UTC);
            DateTimeFormatter startFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss", Locale.UK);
            slackString += "`Started: " + startDateTime.format(startFormatter) + " | ";

            LocalDateTime endDateTime = LocalDateTime.ofEpochSecond(Long.parseLong(timeFinished), 0,ZoneOffset.UTC);
            DateTimeFormatter endFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss", Locale.UK);
            slackString += "Ended: " + endDateTime.format(endFormatter) + "`\n";

            slackString += ">`Duration: " + durationString + " | ";
            slackString += "Transaction Total: " + hitRate + " | ";
            slackString += "Throughput: " + hitsPerSecondString + "`\n";
            slackString += ">`Errors: " + errorRateString + " | ";
            slackString += "Samples: " + hitRate + "`\n";
        }
        else {
            /* We build out each timing point here */
            slackString += "*" + masterName + "*\n>";
            slackString += "`Transaction Total: " + hitRate + " | ";
            slackString += "Throughput: " + hitsPerSecondString + " | ";
            slackString += "Errors: " + errorRateString + " | ";
            slackString += "Samples: " + hitRate + "`\n";
            slackString += ">`90th %ile: " + nthPercentile + " | ";
            slackString += "95th %ile: " + nfthPercentile + " | ";
            slackString += "99th %ile: " + nnthPercentile + " | ";
            slackString += "Max: " + maxTime + "`\n";
        }

        /* Add a newline to the end of each test */
        //slackString += "\n";

        /* Aggregate the output */
        digestSlackMessage.append(slackString);
    }

    public static void createSlackDigestVolumeSmall(String jenkinsURL, String blazemeterURL, String triggerJobName, String triggerJobNumber,
                                                    String jobName, String jobNumber, Integer testEntryCount, String projectName,
                                                    String testId, String testName, String averageTime, String minTime, String nthPercentile,
                                                    String nfthPercentile, String nnthPercentile, String maxTime, String errorRate, String timeStarted,
                                                    String hitRate, String masterName,  String testDuration, String masterId, String workspaceId,
                                                    String projectId, String accountId, String testType, String serviceVersion, String gatewayVersion) {

        /* Initialise some string variable */
        String slackString = "";
        String errorRateString = "";
        String durationString = "";
        String hitsPerSecondString = "";

        /* To build the slack message we need to perform a number of activities, these being */
        if(digestSlackMessage.length() == 0) {
            /* Build the message header */
            /* If the master name is null then we have a problem and we output an error to slack */
            if (masterName != null) {
                /* Firstly we output the the name and status of the current pipeline (the performance one) */
                slackString += ": `" + jobName + "` #" + jobNumber + "\n";
                slackString += "<" + jenkinsURL + "blue/organizations/jenkins/" + jobName + "/detail/" + jobName + "/" + jobNumber + "/pipeline|Click Here To View Pipeline>\n";
                slackString += "Performance Test Triggered By `" + triggerJobName + "` #" + triggerJobNumber + "\n";
                slackString += "<" + blazemeterURL + "accounts/" + accountId + "/workspaces/" + workspaceId + "/projects/" + projectId + "/masters/" + masterId + "/summary|Click Here To View Performance Dashboard>\n";
                //slackString += "Service Version: `" + serviceVersion + "` Gateway Version: `" + gatewayVersion + "`\n";
                /* IF WE CAN GET A LINK TO THE OPENSHIFT REPORTING, CPU MEMORY ETC THEN PUT THE LINK HERE */
                /* ALSO THE QUEUE DEPTH REPORT ALSO PUT THE LINK HERE */
            }
        }

        /****** Block to always get information for each entry in the slack message ********/
        /* Calculate error rate to two decimal places, if any */
        /* Calculate the error rate, if any */
        DecimalFormat df2 = new DecimalFormat(".##");
        /* We are unable to calculate the error percentage if we do not have any data, we check this here */
        if (errorRate != null && hitRate != null) {
            double errorRatePercent = (Double.parseDouble(errorRate) / Double.parseDouble(hitRate)) * 100;
            if (errorRate.equals("0")) {
                errorRateString = "0%";
            } else {
                errorRateString = "" + df2.format(errorRatePercent) + "%";
            }
        } else {
            errorRateString = "0%";
        }


        /* Calculate the transactions per second, to two decimal places */
        /* We are unable to calculate the error percentage if we do not have any data, we check this here */
        double hitsPerSecond = (Double.parseDouble(hitRate) / Double.parseDouble(testDuration));
        if (hitsPerSecond == 0.0) {
            hitsPerSecondString = "";
        } else {
            hitsPerSecondString = "" + df2.format(hitsPerSecond) + "trans/sec";
        }

        /* If the label is ALL then we get the duration and error rates from this array element as it holds the summary */
        /* We build out each timing point here */
        slackString += "*" + testName + "*\n";
        slackString += ">`Transaction Total: " + hitRate + "` ";
        slackString += "`Throughput: " + hitsPerSecondString + "` ";
        slackString += "`Errors: " + errorRateString + "` ";
        slackString += "`Samples: " + hitRate + "`\n";
        slackString += ">`Average: " + averageTime + "` ";
        slackString += "`90th %ile: " + nthPercentile + "` ";
        slackString += "`95th %ile: " + nfthPercentile + "` ";
        slackString += "`99th %ile: " + nnthPercentile + "` ";
        slackString += "`Max: " + maxTime + "`\n";

        /* Add a newline to the end of each test */
        //slackString += "\n";

        /* Aggregate the output */
        digestSlackMessage.append(slackString);
    }

    public static StringBuilder getDigestSlackMessage() {
        /* Return the slack message */
        return digestSlackMessage;
    }
}
