package com.motability.cmer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

public class RequestWorkspaceUserEmail {
    public StringBuilder response;

    /* Public method to return the project id and the workspace id and name */
    public ArrayList<X_WorkspaceUserEmail> RequestWorkspaceUserEmail(String workspaceId) throws ParseException {
        try {
            /* We get the Meganexus workspace users and parse the response for a list of email addresses */
            URL url = new URL("https://a.blazemeter.com/api/v4/workspaces/" + workspaceId + "/users?limit=1000");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            /* If we get a valid response we parse it for the email addresses */
            /* Read buffered response */
            String output;
            response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        /* Parse the response from the request */
        ParseWorkspaceUserEmailResponse parseWorkspaceUserEmail = new ParseWorkspaceUserEmailResponse();
        return parseWorkspaceUserEmail.returnWorkspaceUserEmailArray(response);
    }

    static class ParseWorkspaceUserEmailResponse {

        public ArrayList<X_WorkspaceUserEmail> returnWorkspaceUserEmailArray(StringBuilder response) throws ParseException {

            /* Declare an array list for the project numbers */
            ArrayList<X_WorkspaceUserEmail> wuEmailArray = new ArrayList<X_WorkspaceUserEmail>();

            /* Parse the response */
            JSONObject obj = (JSONObject) new JSONParser().parse(String.valueOf(response));

            /* Get the results array */
            JSONArray resultsArray = (JSONArray) obj.get("result");

            /* Iterate the result array and get the workspaces array */
            for(Object resultObj: resultsArray.toArray()){
                JSONObject result = (JSONObject)resultObj;
                /* Create a new instance of the WorkspaceUserEmail class */
                X_WorkspaceUserEmail wueObject = new X_WorkspaceUserEmail();
                /* Set the values */
                wueObject.setUserEmail(result.get("email").toString());

                /* Add to the array */
                wuEmailArray.add(wueObject);
            }

            /* Return the array */
            return wuEmailArray;
        }
    }
}
