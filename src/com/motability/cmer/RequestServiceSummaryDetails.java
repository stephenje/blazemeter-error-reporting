package com.motability.cmer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

public class RequestServiceSummaryDetails {
    public StringBuilder response;

    /* Public method to return the last test master (report) summary */
    public ArrayList<X_ServiceSummaryDetails> RequestServiceSummaryDetails(String masterId) throws ParseException {
        try {
            /* We get the Meganexus report summary and parse the response for response times, error rate and time of execution */
            URL url = new URL("https://a.blazemeter.com/api/v4/masters/" + masterId + "/reports/default/summary");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            /* If we get a valid response we parse it for the test id's and names */
            /* Read buffered response */
            String output;
            response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        /* Parse the response from the request */
        ParseServiceSummaryDetailsResponse parseServiceSummaryDetails = new ParseServiceSummaryDetailsResponse();
        return parseServiceSummaryDetails.returnServiceSummaryDetailsArray(response);
    }

    static class ParseServiceSummaryDetailsResponse {

        public ArrayList<X_ServiceSummaryDetails> returnServiceSummaryDetailsArray(StringBuilder response) throws ParseException {

            /* Declare an array list for the project numbers */
            ArrayList<X_ServiceSummaryDetails> rsDetailsNameArray = new ArrayList<X_ServiceSummaryDetails>();

            /* Parse the response */
            JSONObject obj = (JSONObject) new JSONParser().parse(String.valueOf(response));

            /* Get the results object */
            JSONObject resultsObj = (JSONObject) obj.get("result");

            /* Iterate the result array and get the summary array */
            JSONArray summaryArray = (JSONArray) resultsObj.get("summary");

            /* As both summary items are identical we get the first and take the report summary details from here */
            /* We need to check we have entries in the summary array, if we do not then we move on */
            if(summaryArray.size() > 0) {
                JSONObject summary = (JSONObject) summaryArray.get(0);
                /* Create a new instance of the X_ServiceSummaryDetails class */
                X_ServiceSummaryDetails rsdObject = new X_ServiceSummaryDetails();
                /* Set the values */
                rsdObject.setAverageResponseTime(summary.get("avg").toString());
                rsdObject.setMinResponseTime(summary.get("min").toString());
                rsdObject.setNinetiethPercentileResponseTime(summary.get("tp90").toString());
                rsdObject.setNinetyFifthPercentileResponseTime(summary.get("tp95").toString());
                rsdObject.setNinetyNinthPercentileResponseTime(summary.get("tp99").toString());
                rsdObject.setMaxResponseTime(summary.get("max").toString());
                rsdObject.setErrorRate(summary.get("failed").toString());
                rsdObject.setTestStartTime(summary.get("first").toString());
                rsdObject.setHitRate(summary.get("hits").toString());
                rsdObject.setTestDuration(summary.get("duration").toString());

                /* Add to the array */
                rsDetailsNameArray.add(rsdObject);
            }
            /* Return the array */
            return rsDetailsNameArray;
        }
    }
}
