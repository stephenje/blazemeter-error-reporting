package com.motability.cmer;

public class X_VolumeSummaryDetails {
    private String averageResponseTime;
    private String minResponseTime;
    private String ninetiethPercentileResponseTime;
    private String ninetyFifthPercentileResponseTime;
    private String ninetyNinthPercentileResponseTime;
    private String maxResponseTime;
    private String errorRate;
    private String testStartTime;
    private String hitRate;
    private String testFinishTime;
    private String testDuration;
    private String testName;

    public String getAverageResponseTime() {
        return this.averageResponseTime;
    }

    public void setAverageResponseTime(String averageResponseTime) {
        this.averageResponseTime = averageResponseTime;
    }

    public String getMinResponseTime() {
        return this.minResponseTime;
    }

    public void setMinResponseTime(String minResponseTime) {
        this.minResponseTime = minResponseTime;
    }

    public String getNinetiethPercentileResponseTime() {
        return this.ninetiethPercentileResponseTime;
    }

    public void setNinetiethPercentileResponseTime(String ninetiethPercentileResponseTime) {
        this.ninetiethPercentileResponseTime = ninetiethPercentileResponseTime;
    }

    public String getNinetyFifthPercentileResponseTime() {
        return this.ninetyFifthPercentileResponseTime;
    }

    public void setNinetyFifthPercentileResponseTime(String ninetyFifthPercentileResponseTime) {
        this.ninetyFifthPercentileResponseTime = ninetyFifthPercentileResponseTime;
    }

    public String getNinetyNinthPercentileResponseTime() {
        return this.ninetyNinthPercentileResponseTime;
    }

    public void setNinetyNinthPercentileResponseTime(String ninetyNinthPercentileResponseTime) {
        this.ninetyNinthPercentileResponseTime= ninetyNinthPercentileResponseTime;
    }

    public String getMaxResponseTime() {
        return this.maxResponseTime;
    }

    public void setMaxResponseTime(String maxResponseTime) {
        this.maxResponseTime = maxResponseTime;
    }

    public String getErrorRate() {
        return this.errorRate;
    }

    public void setErrorRate(String errorRate) {
        this.errorRate = errorRate;
    }

    public String getTestStartTime() {
        return this.testStartTime;
    }

    public void setTestStartTime(String testStartTime) {
        this.testStartTime = testStartTime;
    }

    public String getHitRate() {
        return this.hitRate;
    }

    public void setHitRate(String hitRate) {
        this.hitRate= hitRate;
    }

    public String getTestFinishTime() {
        return this.testFinishTime;
    }

    public void setTestFinishTime(String testFinishTime) {
        this.testFinishTime = testFinishTime;
    }

    public String getTestDuration() {
        return this.testDuration;
    }

    public void setTestDuration(String testDuration) {
        this.testDuration = testDuration;
    }

    public String getTestName() {
        return this.testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
