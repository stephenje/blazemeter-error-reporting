package com.motability.cmer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

public class RequestResponseCodesDistinct {
    public StringBuilder response;

    /* Public method to return the last test master (report) summary */
    public ArrayList<X_ResponseCodesDistinct> RequestResponseCodesDistinct(String masterId) throws ParseException {
        try {
            /* We get the Meganexus report summary and parse the response for response times, error rate and time of execution */
            URL url = new URL("https://a.blazemeter.com/api/v4/masters/" + masterId + "/response-codes/distinct");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            /* If we get a valid response we parse it for the test id's and names */
            /* Read buffered response */
            String output;
            response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        /* Parse the response from the request */
        ParseResponseCodesDistinctResponse parseResponseCodesDistinct = new ParseResponseCodesDistinctResponse();
        return parseResponseCodesDistinct.returnResponseCodesDistinctArray(response);
    }

    static class ParseResponseCodesDistinctResponse {

        public ArrayList<X_ResponseCodesDistinct> returnResponseCodesDistinctArray(StringBuilder response) throws ParseException {

            /* Declare an array list for the project numbers */
            ArrayList<X_ResponseCodesDistinct> rsResponseCodesArray = new ArrayList<X_ResponseCodesDistinct>();

            /* Parse the response */
            JSONObject obj = (JSONObject) new JSONParser().parse(String.valueOf(response));

            /* Get the results array */
            JSONArray resultsArray = (JSONArray) obj.get("result");

            /* Iterate the result array and write the tests to an ArrayList */
            for(Object resultObj: resultsArray.toArray()){
                //JSONObject result = (JSONObject)resultObj;
                /* Create a new instance of the X_ResponseCodesDistinct class */
                X_ResponseCodesDistinct rcdObject = new X_ResponseCodesDistinct();
                /* Set the values */
                rcdObject.setResponseCode(resultObj.toString());

                /* Add to the array */
                rsResponseCodesArray.add(rcdObject);
            }

            /* Return the array */
            return rsResponseCodesArray;
        }
    }
}
