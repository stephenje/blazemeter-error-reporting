package com.motability.cmer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

public class PostMasterNotes {
    public StringBuilder response;

    /* Public method to update the master (report) notes */
    public void PostMasterNotes(String masterId, String serviceVersion, String gatewayVersion) throws ParseException {
        try {
            /* We get the Meganexus tests and parse the response for a list of test id's and names */
            URL url = new URL("https://a.blazemeter.com:443/api/v4/masters/" + masterId);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            //Create JSONObject here
            JSONObject jsonParam = new JSONObject();
            jsonParam.put("note", "Service Version: " + serviceVersion + "\nGateway Version: " + gatewayVersion + "");
            OutputStreamWriter out = new   OutputStreamWriter(conn.getOutputStream());
            out.write(jsonParam.toString());
            out.close();

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
}
