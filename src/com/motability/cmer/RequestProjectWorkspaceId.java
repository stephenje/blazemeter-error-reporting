package com.motability.cmer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class RequestProjectWorkspaceId {
    public StringBuilder response;

    /* Public method to return the project id and the workspace id and name */
    public ArrayList<X_ProjectWorkspaceId> RequestProjectWorkspaceId(String projName) throws ParseException {
        try {
            /* We get the Meganexus accounts and parse the response for a list of project and workspace id's */
            URL url = new URL("https://a.blazemeter.com/api/v4/accounts?limit=1000&tree=true");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            /* If we get a valid response we parse it for the project and workspace id's */
            /* Read buffered response */
            String output;
            response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        /* Parse the response from the request */
        ParseProjectWorkspaceIdResponse parseProjectWorkspaceId = new ParseProjectWorkspaceIdResponse();
        return parseProjectWorkspaceId.returnProjectWorkspaceIdArray(response, projName);
    }

    static class ParseProjectWorkspaceIdResponse {

        public ArrayList<X_ProjectWorkspaceId> returnProjectWorkspaceIdArray(StringBuilder response, String projName) throws ParseException {

            /* Declare an array list for the project numbers */
            ArrayList<X_ProjectWorkspaceId> pwIdArray = new ArrayList<X_ProjectWorkspaceId>();

            /* Parse the response */
            JSONObject obj = (JSONObject) new JSONParser().parse(String.valueOf(response));

            /* Get the results array */
            JSONArray resultsArray = (JSONArray) obj.get("result");

            /* Iterate the result array and get the workspaces array */
            for(Object resultObj: resultsArray.toArray()){
                JSONObject result = (JSONObject)resultObj;

                /* We need to extract the account id from the result so we can use it later in the slack message to display the results URL */
                String accountId = result.get("id").toString();

                JSONArray workspacesArray = (JSONArray) result.get("workspaces");

                /* Iterate the workspaces array and get the projects array */
                for(Object workspacesObj: workspacesArray.toArray()){
                    JSONObject workspaces = (JSONObject) workspacesObj;
                    JSONArray projectsArray = (JSONArray) workspaces.get("projects");

                    /* Iterate the projects array and write the project we pass in to an ArrayList, we use a list as we may at some point need to write more than one */
                    for(Object projectsObj: projectsArray.toArray()){
                        JSONObject projects = (JSONObject) projectsObj;
                        if(projects.get("name").toString().contains(projName)) {
                            /* Create a new instance of the ProjectWorspaceId class */
                            X_ProjectWorkspaceId pwObject = new X_ProjectWorkspaceId();
                            /* Set the values */
                            pwObject.setProjectId(projects.get("id").toString());
                            pwObject.setProjectName(projects.get("name").toString());
                            pwObject.setWorkspaceId(projects.get("workspaceId").toString());
                            pwObject.setAccountId(accountId);
                            /* Add to the array */
                            pwIdArray.add(pwObject);
                        }
                    }
                }
            }

            /* Return the array */
            return pwIdArray;
        }
    }
}
