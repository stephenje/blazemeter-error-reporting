package com.motability.cmer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;

public class RequestMasterIdName {
    public StringBuilder response;

    /* Public method to return the last test master (report) id and name */
    public ArrayList<X_MasterIdName> RequestMasterIdName(String projectId, String workspaceId, String testId, String masterLimit) throws ParseException {
        try {
            /* We get the Meganexus masters (effectively the report groupings) and parse the response for a list of master id's and names, we retrieve the first 2 for comparison in the slack message */
            URL url = new URL("https://a.blazemeter.com/api/v4/masters?projectId=" + projectId + "&workspaceId=" + workspaceId + "&limit=" + masterLimit + "&sort[]=-created&testId=" + testId + "");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            /* We use the API Key and API Secret as Basic Authentication */
            String usernameColonPassword = "" + X_GlobalVariables.BZM_API_KEY + ":" + X_GlobalVariables.BZM_API_SECRET + "";
            String basicAuthPayload = "Basic " + Base64.getEncoder().encodeToString(usernameColonPassword.getBytes());

            // Include the HTTP Basic Authentication payload
            conn.addRequestProperty("Authorization", basicAuthPayload);

            /* Check here for a OK (200) response */
            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            /* If we get a valid response we parse it for the test id's and names */
            /* Read buffered response */
            String output;
            response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            /* Release the connection */
            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }

        /* Parse the response from the request */
        ParseMasterIdNameResponse parseMasterIdName = new ParseMasterIdNameResponse();
        return parseMasterIdName.returnMasterIdNameArray(response);
    }

    static class ParseMasterIdNameResponse {

        public ArrayList<X_MasterIdName> returnMasterIdNameArray(StringBuilder response) throws ParseException {

            /* Declare an array list for the project numbers */
            ArrayList<X_MasterIdName> mIdNameArray = new ArrayList<X_MasterIdName>();

            /* Parse the response */
            JSONObject obj = (JSONObject) new JSONParser().parse(String.valueOf(response));

            /* Get the results array */
            JSONArray resultsArray = (JSONArray) obj.get("result");

            /* Iterate the result array and write the tests to an ArrayList */
            for(Object resultObj: resultsArray.toArray()){
                JSONObject result = (JSONObject)resultObj;
                 /* Create a new instance of the X_MasterIdName class */
                 X_MasterIdName minObject = new X_MasterIdName();
                 /* Set the values */
                 minObject.setMasterId(result.get("id").toString());
                 minObject.setMasterName(result.get("name").toString());
                if(result.containsKey("note")) {
                    minObject.setReportNotes(result.get("note").toString());
                }
                else {
                    minObject.setReportNotes("");
                }

                 /* Add to the array */
                 mIdNameArray.add(minObject);
            }

            /* Return the array */
            return mIdNameArray;
        }
    }
}
